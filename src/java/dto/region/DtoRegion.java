/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.region;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrateur
 */

@XmlRootElement
public class DtoRegion{
    
    
   private String codeRegion;
   private String nomRegion; 
   private Float CaAnnuelReg;
   private int nombreClientReg;

    public String getCodeRegion() {
        return codeRegion;
    }

    public void setCodeRegion(String codeRegion) {
        this.codeRegion = codeRegion;
    }

    public String getNomRegion() {
        return nomRegion;
    }

    public void setNomRegion(String nomRegion) {
        this.nomRegion = nomRegion;
    }

    public Float getCaAnnuelReg() {
        return CaAnnuelReg;
    }

    public void setCaAnnuelReg(Float CaAnnuelReg) {
        this.CaAnnuelReg = CaAnnuelReg;
    }

    public int getNombreClientReg() {
        return nombreClientReg;
    }

    public void setNombreClientReg(int nombreClientReg) {
        this.nombreClientReg = nombreClientReg;
    }

   
   
   
}
   
   