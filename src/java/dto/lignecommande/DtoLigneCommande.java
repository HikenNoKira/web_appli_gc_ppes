package dto.lignecommande;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrateur
 */

@XmlRootElement
public class DtoLigneCommande {
    
    private String refProduit;
    private String designPro;
    private Float qteCommandee;
    private Float PrixUnitaire;
    private Float montantHTLigne;
    private Float montantTTCLigne;

    //<editor-fold defaultstate="collapsed" desc="get & set">
    public String getRefProduit() {
        return refProduit;
    }
    
    public void setRefProduit(String refProduit) {
        this.refProduit = refProduit;
    }
    
    public String getDesignPro() {
        return designPro;
    }
    
    public void setDesignPro(String designPro) {
        this.designPro = designPro;
    }

    public Float getQteCommandee() {
        return qteCommandee;
    }

    public void setQteCommandee(Float qteCommandee) {
        this.qteCommandee = qteCommandee;
    }
    
    
    
    public Float getPrixUnitaire() {
        return PrixUnitaire;
    }
    
    public void setPrixUnitaire(Float PrixUnitaire) {
        this.PrixUnitaire = PrixUnitaire;
    }
    
    public Float getMontantHTLigne() {
        return montantHTLigne;
    }
    
    public void setMontantHTLigne(Float montantHTLigne) {
        this.montantHTLigne = montantHTLigne;
    }
    
    public Float getMontantTTCLigne() {
        return montantTTCLigne;
    }
    
    public void setMontantTTCLigne(Float montantTTCLigne) {
        this.montantTTCLigne = montantTTCLigne;
    }
    
    
    //</editor-fold>


    
    
}
 
   