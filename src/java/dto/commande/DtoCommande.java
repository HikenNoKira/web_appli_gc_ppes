/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.commande;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrateur
 */

@XmlRootElement
public class DtoCommande {
    
    
   private Long  numCom;
   private Long numCli; 
   private String nomCli;
   private String adrCli;
   private String codeReg;
   private String nomRegion;
   private String etatCom;
   private Float montantHT;
   private Float montantTTC;
   private Float totalTVA;

 
   
   
   
    /**
     * @return the numCom
     */
    public Long getNumCom() {
        return numCom;
    }

    /**
     * @param numCom the numCom to set
     */
    public void setNumCom(Long numCom) {
        this.numCom = numCom;
    }

    /**
     * @return the numCli
     */
    public Long getNumCli() {
        return numCli;
    }

    /**
     * @param numCli the numCli to set
     */
    public void setNumCli(Long numCli) {
        this.numCli = numCli;
    }

    /**
     * @return the nomCli
     */
    public String getNomCli() {
        return nomCli;
    }

    /**
     * @param nomCli the nomCli to set
     */
    public void setNomCli(String nomCli) {
        this.nomCli = nomCli;
    }

    /**
     * @return the adrCli
     */
    public String getAdrCli() {
        return adrCli;
    }

    /**
     * @param adrCli the adrCli to set
     */
    public void setAdrCli(String adrCli) {
        this.adrCli = adrCli;
    }

    /**
     * @return the codeReg
     */
    public String getCodeReg() {
        return codeReg;
    }

    /**
     * @param codeReg the codeReg to set
     */
    public void setCodeReg(String codeReg) {
        this.codeReg = codeReg;
    }

    /**
     * @return the nomRegion
     */
    public String getNomRegion() {
        return nomRegion;
    }

    /**
     * @param nomRegion the nomRegion to set
     */
    public void setNomRegion(String nomRegion) {
        this.nomRegion = nomRegion;
    }

    /**
     * @return the etatCom
     */
    public String getEtatCom() {
        return etatCom;
    }

    /**
     * @param etatCom the etatCom to set
     */
    public void setEtatCom(String etatCom) {
        this.etatCom = etatCom;
    }

    public Float getMontantHT() {
        return montantHT;
    }

    public void setMontantHT(Float montantHT) {
        this.montantHT = montantHT;
    }

    public Float getMontantTTC() {
        return montantTTC;
    }

    public void setMontantTTC(Float montantTTC) {
        this.montantTTC = montantTTC;
    }

    public Float getTotalTVA() {
        return totalTVA;
    }

    public void setTotalTVA(Float totalTVA) {
        this.totalTVA = totalTVA;
    }


    }


   
    
    

