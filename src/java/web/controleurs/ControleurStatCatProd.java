package web.controleurs;

import dao.consultation.catprod.DaoCategorieProduit;
import entites.CategorieProduit;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.model.chart.PieChartModel;

/**
 *
 * @author Alexis
 */

@Named
@RequestScoped
public class ControleurStatCatProd {
    
    @Inject DaoCategorieProduit daoCategorieProduit;
    
    private PieChartModel camembert;
    
    @PostConstruct
    public void init(){
        
        System.out.println("init");
        camembert = new PieChartModel();
        camembert.setTitle("Chiffre d'affaire par Catégories de Produits");
        camembert.setLegendPosition("w");
        camembert.setShowDataLabels(true);
        
        for (CategorieProduit cat: daoCategorieProduit.getToutesLesCategoriesProduits()){
          
            camembert.set(cat.getNomCateg(), cat.statCatProd());
        }
    }
    
    public PieChartModel getCamembert(){
        return camembert;
    }
  
}
