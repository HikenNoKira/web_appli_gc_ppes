package web.controleurs;

import bal.Bal;
import dto.ventesmensuelles.ResumeVenteMensuelle;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.inject.Inject;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;

@Model
public class ControleurCaAnnuel {

 @Inject
 private Bal balcommande;
 
 private Float caAnnuel; 
 
 private List<ResumeVenteMensuelle> lesVentesMens=new LinkedList();
 
 private BarChartModel barModel = new BarChartModel();
 private ChartSeries         serie    = new ChartSeries();
 
 
 @PostConstruct
 public void init(){
 
     caAnnuel=balcommande.caAnnuel(utilitaires.UtilDate.anneeCourante());
     
     
    
     for (int m=1;m<=12;m++){
 
        ResumeVenteMensuelle resVM= new ResumeVenteMensuelle();
        resVM.setNumMois(m);
        resVM.setNomMois(utilitaires.UtilDate.nomMois(m));
        resVM.setCa(balcommande.caMensuel(utilitaires.UtilDate.anneeCourante(), m));
        
        lesVentesMens.add(resVM);
        serie.set(utilitaires.UtilDate.nomMois(m),
                  balcommande.caMensuel(utilitaires.UtilDate.anneeCourante(), m) );
 
     }
     
      barModel.addSeries(serie);
     ResumeVenteMensuelle resVM= new ResumeVenteMensuelle();
        
        resVM.setNomMois("Total Année");
        resVM.setCa(balcommande.caAnnuel(utilitaires.UtilDate.anneeCourante()));
        lesVentesMens.add(resVM);
 }

 //<editor-fold defaultstate="collapsed" desc="Getters & Setters">
 
 
   public Float getCaAnnuel() {
        return caAnnuel;
    }

    public void setCaAnnuel(Float caAnnuel) {
        this.caAnnuel = caAnnuel;
    }
 
 
  public BarChartModel getBarModel() {
        return barModel;
    }

    public void setBarModel(BarChartModel barModel) {
        this.barModel = barModel;
    }

 
 //</editor-fold>

}
