/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author Administrateur
 */
@Entity
public class Admin {
   @Id
   private String login;
   private String password;

   //<editor-fold defaultstate="collapsed" desc="comment">
   /**
    * @return the login
    */
   public String getLogin() {
       return login;
   }
   
   /**
    * @param login the login to set
    */
   public void setLogin(String login) {
       this.login = login;
   }
   
   /**
    * @return the password
    */
   public String getPassword() {
       return password;
   }
   
   /**
    * @param password the password to set
    */
   public void setPassword(String password) {
       this.password = password;
   }
   //</editor-fold>
}
