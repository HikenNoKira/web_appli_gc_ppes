package dao.consultation.catprod;

import entites.CategorieProduit;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class DaoCategorieProduitImpl implements DaoCategorieProduit,Serializable {
    
    @PersistenceContext private EntityManager em;
    
    
    @Override
    public List<CategorieProduit> getToutesLesCategoriesProduits(){    
        return em.createQuery("Select c from CategorieProduit c").getResultList();
    }

    //<editor-fold defaultstate="collapsed" desc="GETTER ET SETTER">
    public EntityManager getEm() {
        return em;
    }
    
    public void setEm(EntityManager em) {
        this.em = em;
    }
//</editor-fold>
    
    
    
}
