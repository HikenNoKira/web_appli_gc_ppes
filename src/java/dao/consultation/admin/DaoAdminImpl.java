/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.consultation.admin;

import entites.Admin;
import java.io.Serializable;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Administrateur
 */
public class DaoAdminImpl implements DaoAdmin, Serializable {

    @PersistenceContext private EntityManager em;
    @Override
    public Admin getAdmin(String login) {
        return em.find(Admin.class, login);  
    }
    
}
