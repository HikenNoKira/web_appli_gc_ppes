package dao.consultation.commande;
import entites.Commande;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
public class DaoCommandeImpl   implements DaoCommande, Serializable{

    @PersistenceContext private EntityManager em;
    
    @Override
    public Commande getCommande(Long pNumCom) {
        return em.find(Commande.class, pNumCom);
    }

    @Override
    public List<Commande> getToutesLesCommandes() {
        return em.createQuery("Select c from Commande c").getResultList();
    }   

   
    @Override
    public List<Commande> getLesCommandes( int pAnnee) {
      
       String reqJPQL = "Select com "+
                        "From   Commande com "+
                        "Where  FUNC('YEAR',com.dateCom)= :pA"; 
       
       
        Query qryListeCom= em.createQuery(reqJPQL);
       
        qryListeCom.setParameter("pA", pAnnee);
       
        return qryListeCom.getResultList();
    }
    
    
   
    @Override
    public List<Commande> getLesCommandes( int pAnnee, String pEtat) {
      
       String reqJPQL = "Select com "+
                        "From   Commande com "+
                        "Where  FUNC('YEAR',com.dateCom)= :pA " +
                        "And    com.etatCom=:pE"; 
       
       
       
        Query qryListeCom= em.createQuery(reqJPQL);
       
        qryListeCom.setParameter("pA", pAnnee);
        qryListeCom.setParameter("pE", pEtat);
       
        return qryListeCom.getResultList();
    }
    
    
     
    @Override
    public List<Commande> getLesCommandes( int pAnnee, int pMois) {
      
       String reqJPQL = "Select com "+
                        "From   Commande com "+
                        "Where  FUNC('YEAR' ,com.dateCom)= :pA " +
                        "And    FUNC('MONTH',com.dateCom)= :pM" 
                         ; 
          
       Query qryListeCom= em.createQuery(reqJPQL);
       
       qryListeCom.setParameter("pA", pAnnee);
       qryListeCom.setParameter("pM", pMois);
       
       return qryListeCom.getResultList();
    }
    
 
     
     
  
    
    public List<Commande> getLesCommandes( int pAnnee, int pMois, String pEtat) {
      
       String reqJPQL = "Select com "+
                        "From   Commande com "+
                        "Where  FUNC('YEAR' ,com.dateCom)= :pA " +
                        "And    FUNC('MONTH',com.dateCom)= :pM " +
                        "And    com.etatCom=:pE"  ; 
          
       Query qryListeCom= em.createQuery(reqJPQL);
       
       qryListeCom.setParameter("pA", pAnnee);
       qryListeCom.setParameter("pM", pMois);
       qryListeCom.setParameter("pE", pEtat);
       
       return qryListeCom.getResultList();
    }
    

}
