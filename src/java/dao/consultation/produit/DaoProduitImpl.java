package dao.consultation.produit;
import entites.Produit;
import java.io.Serializable;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class DaoProduitImpl  implements DaoProduit, Serializable{

    @PersistenceContext private EntityManager em;
    
    @Override
    public Produit getProduit(String pRefProd) {
        return em.find(Produit.class, pRefProd);
    }    
}
