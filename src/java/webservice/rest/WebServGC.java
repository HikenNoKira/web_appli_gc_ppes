package webservice.rest;



import dto.commande.DtoCommande;
import dto.lignecommande.DtoLigneCommande;
import dto.region.DtoRegion;
import entites.Admin;
import entites.Commande;
import entites.LigneDeCommande;
import entites.Region;
import java.util.LinkedList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

@Stateless
@Path("gc")
public class WebServGC  {
    
    ///////////////////////////////////////////////////////////////
    
   
    @Inject dao.consultation.commande.DaoCommande daocom;
    @Inject dao.consultation.region.DaoRegion daoreg;
    @Inject dao.consultation.admin.DaoAdmin daoA;
    
    
    /////////////////////////////////////////////////////////////// 
    
    
    @GET
    @Path("resumecommande/{numCom}")
    @Produces({"application/xml","application/json"})
     public DtoCommande getLaCommande(@PathParam("numCom") Long numcom) {
         
        
       DtoCommande dto = new DtoCommande(); 
       Commande cmd= daocom.getCommande(numcom);
       
       dto.setNumCli(cmd.getLeClient().getNumCli());
       dto.setNomCli(cmd.getLeClient().getNomCli());
       dto.setAdrCli(cmd.getLeClient().getAdrCli());
       dto.setCodeReg(cmd.getLeClient().getLaRegion().getCodeRegion());
       dto.setNomRegion(cmd.getLeClient().getLaRegion().getNomRegion());
       dto.setEtatCom(cmd.getEtatCom());
       dto.setMontantHT(cmd.montantCommandeHT());
       dto.setMontantTTC(cmd.montantCommandeTTC());
       dto.setTotalTVA(cmd.montantTVA());

       return dto;
       
    }
    
    @GET
    @Path("resumelignesdecommande/{numCom}")
    @Produces({"application/xml","application/json"})
     public List<DtoLigneCommande> getLesLignesdeCommande(@PathParam("numCom") Long numcom) {
         
        
          List<DtoLigneCommande> liste= new LinkedList();
         Commande cmd= daocom.getCommande(numcom);
         
         
         for(LigneDeCommande ldc : cmd.getLesLignesDeCommande()){
             DtoLigneCommande dto = new DtoLigneCommande();   
             dto.setRefProduit(ldc.getLeProduit().getRefProd());
             dto.setDesignPro(ldc.getLeProduit().getDesigProd());
             dto.setQteCommandee(ldc.getQteCom());
             dto.setPrixUnitaire(ldc.getLeProduit().getPrixProd());
             dto.setMontantHTLigne(ldc.getLaCommande().montantCommandeHT());
             dto.setMontantTTCLigne(ldc.getLaCommande().montantCommandeTTC());
             
             liste.add(dto);
         }
        

       return liste;
       
    }


    @GET
    @Path("resumeregion/{codeRegion}")
    @Produces({"application/xml","application/json"})
     public DtoRegion getLaRegion(@PathParam("codeRegion") String codeRegion) {
         
        
       DtoRegion dtoreg = new DtoRegion(); 
       Region reg= daoreg.getRegion(codeRegion);
       
       dtoreg.setNomRegion(reg.getNomRegion());
       dtoreg.setCaAnnuelReg(reg.caAnneeEnCours());
       dtoreg.setNombreClientReg(reg.getLesClients().size());
       
 
       return dtoreg;
       
    }
    
    
    
    @GET
    @Path("resumelesRegions")
    @Produces({"application/xml","application/json"})
     public List<DtoRegion> getToutesLesRegions()  {
         
        
         List<DtoRegion> liste = new LinkedList <DtoRegion>();
         
         List<Region> reg= daoreg.getToutesLesRegions();
         
         
         for(Region laRegion : reg){
            
             DtoRegion dto = new DtoRegion();   
             dto.setNomRegion(laRegion.getNomRegion());
             dto.setCaAnnuelReg(laRegion.caAnneeEnCours());
             dto.setNombreClientReg(laRegion.getLesClients().size());
            
             
             liste.add(dto);
         }
        

       return liste;
       
    }
    
    @GET
    @Path("login/{login}/password")
    @Produces({"text/plain"})
    public String login(@PathParam("login")String login, @PathParam("password")String password){
                
        Admin a = daoA.getAdmin(login);
        
        if(a.getLogin().equals(login) && a.getPassword().equals(password)){
            return "You're a challenger !";
        }else{
            return "You're a noob.";
        }
        
    }
    
    @GET
    @Path("client/login/{mail}/password")
    @Produces({"text/plain"})
    public String logincli(@PathParam("mail")String mail, @PathParam("password")String password){
        return null;       
    }
    
}
